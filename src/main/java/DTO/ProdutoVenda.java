/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author igor
 */
public class ProdutoVenda {
    
    private Venda venda; 
    
    private Produto produto; 
    
    private DoubleProperty quantidade; 

    public ProdutoVenda(Venda venda, Produto produto, Double quantidade){
        this.venda = venda; 
        this.produto = produto;
        this.quantidade = new SimpleDoubleProperty(quantidade);
        
    }
    
    
    /**
     * @return the venda
     */
    public Venda getVenda() {
        return venda;
    }

    /**
     * @param venda the venda to set
     */
    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the quantidade
     */
    public Double getQuantidade() {
        return quantidade.get();
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(Double quantidade) {
        this.quantidade.set(quantidade);
    }
    
    public DoubleProperty getQuantidadeProp(){
        return this.quantidade; 
    }
    
}
