/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;


import java.io.Serializable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javax.xml.bind.annotation.XmlIDREF;


/**
 *
 * @author igor
 */

public class Estoque implements AbstractDto<Integer>,Serializable {

    
    private IntegerProperty id; 
    

    private Produto produto;
    

    private DoubleProperty quantidade; 
    
    private final DoubleProperty quantAtual; 

    public Estoque(Double quantidade, Double quantAtual, Integer id){
        this.quantidade = new SimpleDoubleProperty(quantidade);
        this.quantAtual = new SimpleDoubleProperty(quantAtual);
        this.id = new SimpleIntegerProperty(id);
    }
    
    public Integer getId() {
        return this.id.get(); 
    }
    
      public IntegerProperty getIdProp() {
        return this.id; 
    }

    /**
     * @param id the id to set
     */
    public void setIdProperty(IntegerProperty id) {
        this.id = id;
    }
    
    public void setId(Integer id){
        this.id.set(id);
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    
    public void setProduto(Produto produto) {
        this.produto = produto;
    }


    public double getQuantidade(){
       return this.quantidade.get();
    }
    /**
     * @return the quantidade
     */
    public DoubleProperty getQuant() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(DoubleProperty quantidade) throws Exception {
           this.quantidade = quantidade;  

    }
    public void setQuantidade(Double quantidade) throws Exception{
           this.quantidade.set(quantidade);  


    }

    /**
     * @return the quantAtual
     */
    public DoubleProperty getQuantAtualProperty() {
        return quantAtual;
    }

    /**
     * @param quantAtual the quantAtual to set
     */

    
    public Double getQuantAtual() {
        return quantAtual.get();
    }

    /**
     * @param quantAtual the quantAtual to set
     */
    public void setQuantAtual(Double quantAtual) {
        this.quantAtual.set(quantAtual);
    }

    /**
     * @param id the id to set
     */
    }
    
   
