/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author igor
 */

public class Venda implements AbstractDto<Integer>,Serializable {

    private IntegerProperty id;

    private DoubleProperty total;
    
    

    public Venda(Integer id, Double total){
        this.id = new SimpleIntegerProperty(id);
        this.total = new SimpleDoubleProperty(total);
    }
    
    public Integer getId() {
        return this.id.get();
    }
    
    public IntegerProperty getIdProp(){
        return id;
    }

    /**
     * @param id the id to set
     */
    
    public void setId(Integer id){
        this.id.set(id);
    }
    public void setIdProp(IntegerProperty id) {
        this.id = id;
    }

    /**
     * @return the produto
     */


    /**
     * @return the quantidade
     */

    /**
     * @return the total
     */
    public Double getTotal(){
        return this.total.get();
    }
    public DoubleProperty getTotalProp() {
        return total;
    }

    /**
     * @param total the total to set
     */
    
    public void setTotal(Double total){
        this.total.set(total);
    }
    
    public void setTotalProp(DoubleProperty total) {
        this.total = total;
    }

    /**
     * @return the produto
     */

    
    
}
