/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Estoque;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class EstoqueController implements Initializable {

    Main main;
    
    @FXML
    private TableView<Estoque> estoque; 
    
    @FXML
    private TableColumn<Estoque, Number> codigo; 
    
    @FXML
    private TableColumn<Estoque, String> descricao; 
    
    @FXML
    private TableColumn<Estoque, Number> quantidade; 
    
    @FXML
    private TableColumn<Estoque, Number> quantAtual; 
    
    @FXML
    private TextField busca; 
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigo.setCellValueFactory(elt -> elt.getValue().getIdProp());
        descricao.setCellValueFactory(elt -> elt.getValue().getProduto().getDescricaoProp());
        quantidade.setCellValueFactory(elt -> elt.getValue().getQuant());
        quantAtual.setCellValueFactory(elt -> elt.getValue().getQuantAtualProperty());
    }    
 
        public void setMain(Main main) {
        this.main = main;
        estoque.setItems(main.getEstoqueData());
    }
        
        @FXML
        public void handleKeyRelease(){
            String produto = busca.getText();
            ObservableList<Estoque> tmp = FXCollections.observableArrayList();
            
            for (Estoque i : main.getEstoqueData()) {
                String nomeTmp = i.getProduto().getDescricao().toLowerCase();
                if(nomeTmp.equals(produto));
                tmp.add(i);
            }
            estoque.setItems(tmp);
        }
        
}
