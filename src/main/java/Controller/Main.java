package Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import DTO.Estoque;
import DTO.Produto;
import DTO.ProdutoVenda;
import DTO.ProdutoWrapper;
import DTO.Venda;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author igor
 */
public class Main extends Application {

    Main main;
    
   private ObservableList<Produto> produtoData = FXCollections.observableArrayList();
    
   private ObservableList<Estoque> estoqueData = FXCollections.observableArrayList();
    
   private ObservableList<Venda> vendaData = FXCollections.observableArrayList();
   
   private ObservableList<Produto> carrinhoData = FXCollections.observableArrayList();
   
   private ObservableList<Venda> vendaAtualData = FXCollections.observableArrayList();
   
   private ObservableList<ProdutoVenda> produtoVendaData = FXCollections.observableArrayList();
   
   private ObservableList<ProdutoVenda> tempProdutoVendaData = FXCollections.observableArrayList();
   
  
    
   private final String title = "Trabalho Final";

    public Stage primaryStage;
    public BorderPane mainView;
    
     public static void main(String[] args) {
        try {
            launch(args);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(title);

        // initializes primary layout
        initMainView();

        //handleLogin("admin", "admin");
        setScene(loadLoginView());
        
    }
    
        public void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }
        

        //primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("res/images/icon.png")));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Menu.fxml"));
        
        mainView = loader.load();

        ((MenuController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Login.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }
    public Parent loadProduto() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Produto.fxml"));

        Parent root = loader.load();

        ((ProdutoController) loader.getController()).setMain(this);

        return root;
    }
    
        public Parent loadAbout() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/About.fxml"));

        Parent root = loader.load();

        ((MenuController) loader.getController()).setMain(this);

        return root;
    }
    
        public Parent loadProdutoVenda() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/VendaProduto.fxml"));

        Parent root = loader.load();

        ((ProdutoVendaController) loader.getController()).setMain(this);

        return root;
    }
    
    public Parent loadCarrinho() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Carrinho.fxml"));

        Parent root = loader.load();

        ((VendaProdutoController) loader.getController()).setMain(this);

        return root;
    }
    
        public Parent loadVenda() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Venda.fxml"));

        Parent root = loader.load();

        ((VendaController) loader.getController()).setMain(this);

        return root;
    }
    
    public Parent loadEstoque() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Estoque.fxml"));

        Parent root = loader.load();

        ((EstoqueController) loader.getController()).setMain(this);

        return root;
    }
    
        public void loadProdutoNew() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/ProdutoNew.fxml"));

        Parent root = loader.load();

        ((ProdutoEditController) loader.getController()).setMain(this);

            
        
    }
        
    public void importar(File file) throws JAXBException {
        try{
        JAXBContext contexto = JAXBContext.newInstance(ProdutoWrapper.class);
        Unmarshaller um = contexto.createUnmarshaller();

        ProdutoWrapper wrapper = (ProdutoWrapper) um.unmarshal(file);
        main.getProdutoData().addAll(wrapper.getProdutos());
        }catch(JAXBException jb){
            file = null;
            System.out.println(jb);
        }
       
        
    }

    public File getProdutoFile(){
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String path = prefs.get("file",null);
        if(path != null){
            return new File(path);
        }else{
            return null;
        }
    }
    
    
    public void salvarProd(File file) throws JAXBException{
        try{
            JAXBContext contexto = JAXBContext.newInstance(ProdutoWrapper.class);
            Marshaller m = contexto.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            
            ProdutoWrapper wrapper = new ProdutoWrapper();
            wrapper.setProdutos(produtoData);
            m.marshal(wrapper, file);
            
        }catch(JAXBException jb){
            jb.printStackTrace();
        }
    }
   

    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadProduto());
                setScene(mainView);
               //initMainView();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
    
    public void voltaMain() throws IOException{
        initMainView();
    }
    
    public ObservableList<Produto> getProdutoData() {
        return produtoData;
    }

    /**
     * @param produtoData the produtoData to set
     */
    public void setProdutoData(ObservableList<Produto> produtoData) {
        this.produtoData = produtoData;
    }

    /**
     * @return the estoqueData
     */
    public ObservableList<Estoque> getEstoqueData() {
        return estoqueData;
    }

    /**
     * @param estoqueData the estoqueData to set
     */
    public void setEstoqueData(ObservableList<Estoque> estoqueData) {
        this.estoqueData = estoqueData;
    }

    /**
     * @return the vendaData
     */
    public ObservableList<Venda> getVendaData() {
        return vendaData;
    }

    /**
     * @param vendaData the vendaData to set
     */
    public void setVendaData(ObservableList<Venda> vendaData) {
        this.vendaData = vendaData;
    }

    /**
     * @return the carrinhoData
     */
    public ObservableList<Produto> getCarrinhoData() {
        return carrinhoData;
    }

    /**
     * @param carrinhoData the carrinhoData to set
     */
    public void setCarrinhoData(ObservableList<Produto> carrinhoData) {
        this.carrinhoData = carrinhoData;
    }

    /**
     * @return the vendaAtualData
     */
    public ObservableList<Venda> getVendaAtualData() {
        return vendaAtualData;
    }

    /**
     * @param vendaAtualData the vendaAtualData to set
     */
    public void setVendaAtualData(ObservableList<Venda> vendaAtualData) {
        this.vendaAtualData = vendaAtualData;
    }

    /**
     * @return the produtoVendaData
     */
    public ObservableList<ProdutoVenda> getProdutoVendaData() {
        return produtoVendaData;
    }

    /**
     * @param produtoVendaData the produtoVendaData to set
     */
    public void setProdutoVendaData(ObservableList<ProdutoVenda> produtoVendaData) {
        this.produtoVendaData = produtoVendaData;
    }

    /**
     * @return the tempProdutoVendaData
     */
    public ObservableList<ProdutoVenda> getTempProdutoVendaData() {
        return tempProdutoVendaData;
    }

    /**
     * @param tempProdutoVendaData the tempProdutoVendaData to set
     */
    public void setTempProdutoVendaData(ObservableList<ProdutoVenda> tempProdutoVendaData) {
        this.tempProdutoVendaData = tempProdutoVendaData;
    }


    
}
   
