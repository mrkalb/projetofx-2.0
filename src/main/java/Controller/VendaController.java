/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.AlertBox;
import DTO.Estoque;
import DTO.Produto;
import DTO.ProdutoVenda;
import DTO.Venda;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class VendaController implements Initializable {

    Main main;

    @FXML
    private TableView<Produto> produto;

    @FXML
    private TableColumn<Produto, Number> codigo;

    @FXML
    private TableColumn<Produto, String> descricao;

    @FXML
    private TableColumn<Produto, Number> quantidade;

    @FXML
    private TableColumn<Produto, Number> preco;

    @FXML
    private TableView<Venda> venda;

    @FXML
    private TableColumn<Venda, Integer> nrvenda;

    @FXML
    private TableColumn<Venda, Number> total;

    @FXML
    private TextField quant;

    public static boolean bool = false;

    private boolean teste = false;

    public static boolean finalizado = false;

    public static boolean car = false;

    public static boolean criado = false;

    public Random random = new Random();

    private Integer id;

    private Venda v;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigo.setCellValueFactory(elt -> elt.getValue().getIdProperty());
        descricao.setCellValueFactory(elt -> elt.getValue().getDescricaoProp());
        preco.setCellValueFactory(elt -> elt.getValue().getPreçoUnitarioProp());
        quantidade.setCellValueFactory(elt -> elt.getValue().getEstoque().getQuantAtualProperty());
        if (bool) {
            nrvenda.setCellValueFactory(elt -> elt.getValue().getIdProp().asObject());
            total.setCellValueFactory(elt -> elt.getValue().getTotalProp());
        }
    }

    public void setMain(Main main) {
        this.main = main;
        produto.setItems(main.getProdutoData());
        if (finalizado) {
            venda.setItems(main.getVendaData());
        }

    }

    @FXML
    public void adicionaProd() throws IOException {

        int itemIndex = produto.getSelectionModel().getSelectedIndex();
        try{
        if (quant.getText().isEmpty()) {
            AlertBox.display("Digite uma quantidade", "Digite por favor a quantidade desejada");
            teste = true;
        }
        if (main.getProdutoData().get(itemIndex).getEstoque().getQuantAtual() <= 0) {
            AlertBox.display("Estoque zerado", "O estoque do produto desejado esta zerado,\nselecione outro produto");
            teste = true;
        }
        if ((main.getProdutoData().get(itemIndex).getEstoque().getQuantAtual() - parseDouble(quant.getText())) < 0) {
            AlertBox.display("Estoque zerado", "O estoque irá ficar abaixo de 0\n, diminua as quantidades");
            teste = true;
        }

        if (!criado && !teste) {
            id = random.nextInt(120);
            v = new Venda(id, 0.0);
            criado = true;
        }
        
        if (itemIndex >= 0 && itemIndex < produto.getItems().size() && !teste) {
            main.getCarrinhoData().addAll(produto.getItems().get(itemIndex));
            ProdutoVenda prodVenda = new ProdutoVenda(v, produto.getItems().get(itemIndex), parseDouble(quant.getText()));
            main.getProdutoVendaData().addAll(prodVenda);
        }

        car = true;
        bool = true;
        }catch(ArrayIndexOutOfBoundsException e){
            AlertBox.display("Erro", "Selecione um produto por favor");
        }
    }

    @FXML
    public void carrinho() throws IOException {
        if (!quant.getText().isEmpty() && !main.getCarrinhoData().isEmpty()) {
            carrinhoCalcula();
            main.mainView.setCenter(main.loadCarrinho());
        }
        if(main.getCarrinhoData().isEmpty()){
            AlertBox.display("O carrinho está zerado", "Nao tem nada para mostrar");
        }
        main.mainView.setCenter(main.loadCarrinho());
        
    }

    public void carrinhoCalcula() throws IOException {
        /*venda.setTotal(venda.getQuantidade() * main.getProdutoData().get(itemIndex).getPreçoUnitario());
           main.getProdutoData().get(itemIndex).getEstoque()
                   .setQuantAtual(main.getProdutoData().get(itemIndex).
                           getQuantidadeDo() - parseDouble(quant.getText())); 
                venda.setProduto(main.getCarrinhoData());
        venda.setIndice(itemIndex);
        main.getVendaData().addAll(venda);*/
        double totala = 0.0;

        if (car) {
            for (int i = 0; i < main.getCarrinhoData().size(); i++) {
                totala += (main.getProdutoVendaData().get(i).getQuantidade() * main.getProdutoVendaData().get(i).getProduto().getPreçoUnitario());
                main.getProdutoVendaData().get(i).getProduto().getEstoque()
                        .setQuantAtual(main.getProdutoVendaData().get(i).getProduto().getEstoque().getQuantAtual() - parseDouble(quant.getText()));

            }
            v.setTotal(totala);
            main.getVendaAtualData().addAll(v);
            main.getVendaData().addAll(v);
        }

    }

    @FXML
    public void produtoVenda() throws IOException {
        int itemIndex = this.venda.getSelectionModel().getSelectedIndex();
        // main.getCarrinhoData().addAll(main.getVendaData().get(itemIndex).getProduto());
        try{
        for(int i =0;i< main.getProdutoVendaData().size();i++){
            if(main.getProdutoVendaData().get(i).getVenda().getId() == main.getVendaData().get(itemIndex).getId()){
                main.getTempProdutoVendaData().addAll(main.getProdutoVendaData().get(i));
        }
        }
        main.mainView.setCenter(main.loadProdutoVenda());
        }catch(ArrayIndexOutOfBoundsException e){
            AlertBox.display("Error", "Selecione uma venda");
        }
    }

    /*public double calcula(){
        double valor = 0.0; 
        for (Venda prod : main.getVendaData()) { 
                 valor +=  (prod.getQuantidade() * prod.getProduto().getPreçoUnitario()); 
        }
            return valor;
    }*/
    @FXML
    public void deletar() {
        int itemIndex = venda.getSelectionModel().getSelectedIndex();
        if (itemIndex >= 0 && itemIndex < venda.getItems().size()) {
            for(int i=0; i< main.getProdutoVendaData().size();i++){
                if(main.getProdutoVendaData().get(i).getVenda().getId() == venda.getItems().get(itemIndex).getId()){
                    main.getProdutoVendaData().get(i).getProduto().getEstoque().setQuantAtual(main.getProdutoVendaData().get(i).getProduto().getQuantidadeDo());
                }
            }
            venda.getItems().remove(itemIndex);
        }
    }
}
