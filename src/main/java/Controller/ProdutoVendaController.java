/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Estoque;
import DTO.Produto;
import DTO.ProdutoVenda;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author igor
 */
public class ProdutoVendaController implements Initializable {

    Main main;

    @FXML
    private TableView<ProdutoVenda> produto;
    
    @FXML
    private TableView<ProdutoVenda> venda; 

    @FXML
    private TableColumn<ProdutoVenda, String> descricao;

    @FXML
    private TableColumn<ProdutoVenda, Number> total;

    @FXML
    private TableColumn<ProdutoVenda, Number> codVenda;

    @FXML
    private TableColumn<ProdutoVenda, Number> codProd;

    @FXML
    private TableColumn<ProdutoVenda, Number> quant;

    
    private Stage mWindow;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        descricao.setCellValueFactory(elt -> elt.getValue().getProduto().getDescricaoProp());
        total.setCellValueFactory(elt -> elt.getValue().getVenda().getTotalProp());
        codVenda.setCellValueFactory(elt -> elt.getValue().getVenda().getIdProp());
        codProd.setCellValueFactory(elt -> elt.getValue().getProduto().getIdProperty());
        quant.setCellValueFactory(elt -> elt.getValue().getQuantidadeProp());
    }

    public void setMain(Main main) {
        this.main = main;
        venda.setItems(main.getTempProdutoVendaData());
        produto.setItems(main.getTempProdutoVendaData());
    }

    public void setStage(Stage window) {
        mWindow = window;
    }

}
