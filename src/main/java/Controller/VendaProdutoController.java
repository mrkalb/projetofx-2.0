/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Estoque;
import DTO.Produto;
import DTO.ProdutoVenda;
import DTO.Venda;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author igor
 */
public class VendaProdutoController implements Initializable {

    Main main;

    @FXML
    private TableView<Venda> venda;

    @FXML
    private TableView<Produto> produto;

    @FXML
    private TableColumn<Venda, Number> id; 
    
    @FXML
    private TableColumn<Venda, Number> total;

    @FXML
    private TableColumn<Produto, String> descricao;
    
    @FXML
    private TableColumn<Produto, Number> preco;  



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setCellValueFactory(elt -> elt.getValue().getIdProp());
        total.setCellValueFactory(elt -> elt.getValue().getTotalProp());
        descricao.setCellValueFactory(elt -> elt.getValue().getDescricaoProp());
        preco.setCellValueFactory(elt -> elt.getValue().getPreçoUnitarioProp());
    }

    public void setMain(Main main) {
        this.main = main;
        venda.setItems(main.getVendaAtualData());
        produto.setItems(main.getCarrinhoData());
    }

    @FXML
    public void finalizado() throws IOException {
        VendaController.finalizado = true;
        VendaController.bool = true;
        VendaController.criado = false;
        main.getCarrinhoData().clear();
        main.getVendaAtualData().clear();
        VendaController.car = false;
        main.mainView.setCenter(main.loadVenda());

    }

    @FXML
    public void handleDelete() throws IOException {
        int itemIndex = produto.getSelectionModel().getSelectedIndex();
        Venda itemIndex1 = venda.getSelectionModel().getSelectedItem();
        Produto prd = produto.getSelectionModel().getSelectedItem();
        if (itemIndex >= 0 && itemIndex < produto.getItems().size()) {
            main.getCarrinhoData().get(itemIndex).getEstoque().setQuantAtual(main.getCarrinhoData().get(itemIndex).getQuantidadeDo());
            produto.getItems().remove(itemIndex);
            main.getCarrinhoData().remove(prd);
            for(int i = 0; i< main.getProdutoVendaData().size();i++){
                if(main.getProdutoVendaData().get(i).getProduto().equals(prd))
                main.getProdutoVendaData().remove(i);
            }
        }
        if(main.getCarrinhoData().isEmpty() && main.getVendaData().size() == 1){
            VendaController.bool = false; 
            VendaController.car = false; 
            VendaController.criado = false; 
            main.getVendaAtualData().clear();
            main.getVendaData().clear();
        }

            main.mainView.setCenter(main.loadVenda());

        }

 //   }
    
    
    

}
