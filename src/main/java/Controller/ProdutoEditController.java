/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Estoque;
import DTO.Produto;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author igor
 */
public class ProdutoEditController implements Initializable {

    Main main;

    @FXML
    private TextField descricao;

    @FXML
    private TextField preco;

    @FXML
    private TextField quantidade;

    @FXML
    private Button ok;

    @FXML
    private Button cancel;

    Random numero = new Random();

    // private final ProdutoDao prDao = new ProdutoDao(PersistenceManager.getEntityManager());; 
    //private final EstoqueDao estDao; 
    private Produto mProd;
    private Estoque mEst;
    private Stage mWindow;
    private boolean clicou;

    public ProdutoEditController() {
        //this.estDao = new EstoqueDao(PersistenceManager.getEntityManager());
    }

    public static boolean display(Produto p, Estoque e) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/ProdutoNew.fxml"));

            Parent node = loader.load();
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            Scene scene = new Scene(node);
            window.setScene(scene);
            ProdutoEditController controller = loader.getController();
            controller.setProduto(p, e);
            controller.setStage(window);
            window.showAndWait();

            return controller.clicou;

        } catch (IOException ef) {
            System.out.println("erro" + ef.toString());
        }
        return false;
    }

    public void setProduto(Produto p, Estoque e) {

        mProd = p;
        mEst = e;

        descricao.setText(p.getDescricao());
        preco.setText(String.valueOf(p.getPreçoUnitario()));
        quantidade.setText(String.valueOf(e.getQuantidade()));

    }

    
    @FXML
    public void handleOk() throws Exception {

        mProd.setDescricao(descricao.getText());
        mProd.setPreco(parseDouble(preco.getText()));
        mEst.setProduto(mProd);
        mProd.setEstoque(mEst);
        mProd.setId(numero.nextInt(120));
        mEst.setQuantidade(parseDouble(quantidade.getText()));
        mProd.setQuantid(parseDouble(quantidade.getText()));
        mEst.setQuantAtual(mProd.getQuantidadeDo());
        mEst.setId(numero.nextInt(120));
        clicou = true;
        mWindow.close();

        /*
       prDao.startTransaction();
       mProd.setDescricao(descricao.getText());
       mProd.setPreco(parseDouble(preco.getText()));
       prDao.save(mProd);
       prDao.commitTransaction();
       estDao.startTransaction();
       estoque.setProduto(prDao.getById(p));
       estoque.setQuantidade(parseDouble(quantidade.getText()));
       estDao.save(estoque);
       estDao.commitTransaction();
       mWindow.close();*/
    }

    @FXML
    public void handleCancel() {
        clicou = false;
        mWindow.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setMain(Main main) {
        this.main = main;
    }

    public void setStage(Stage window) {
        mWindow = window;
    }
}
