/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.AlertBox;
import DTO.Estoque;
import DTO.Produto;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class ProdutoController implements Initializable {
    
    Main main;
    
    ProdutoEditController pE;
    
    @FXML
    private TableView<Produto> produto;    
    
    @FXML
    private TableColumn<Produto, Number> codigo;    
    
    @FXML
    private TableColumn<Produto, String> descricao;
    
    @FXML
    private TableColumn<Produto, Number> preco;    
    @FXML
    private TableColumn<Produto, Number> estInicial;    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigo.setCellValueFactory(elt -> elt.getValue().getIdProperty());
        descricao.setCellValueFactory(elt -> elt.getValue().getDescricaoProp());
        preco.setCellValueFactory(elt -> elt.getValue().getPreçoUnitarioProp());
        estInicial.setCellValueFactory(elt -> elt.getValue().getQuantidade());
    }
    
    @FXML
    public void criar() throws IOException {        
        Produto p = new Produto("", 0.0, 0.0, 0);
        Estoque e = new Estoque(0.0, 0.0, 0);
        
        boolean temp = ProdutoEditController.display(p, e);
        
        if (temp) {
            main.getProdutoData().add(p);
            main.getEstoqueData().add(e);
            
        }
    }
    
    @FXML
    public void deletar(){
        int itemIndex = produto.getSelectionModel().getSelectedIndex();
        Estoque p = produto.getSelectionModel().getSelectedItem().getEstoque();
        if (itemIndex >= 0 && itemIndex < produto.getItems().size()){
            produto.getItems().remove(itemIndex);
            main.getEstoqueData().remove(p);
        }
    }
    /**
     * Initializes the controller class.
     */
    public void setMain(Main main) {
        this.main = main;
        produto.setItems(main.getProdutoData());
    }
    
    @FXML
    public void editar() {
        
        int itemIndex = produto.getSelectionModel().getSelectedIndex();
        
        if (itemIndex >= 0 && itemIndex < produto.getItems().size()) {
            Produto p = main.getProdutoData().get(itemIndex);
            Estoque e = main.getEstoqueData().get(itemIndex);
            
            boolean clicked = ProdutoEditController.display(p,e);
        }

        

    }
}
