/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class LoginController implements Initializable {
    Main main;

    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passTextField;

    @FXML
    private Label errorLabel;

    
    public void doLogin() {
        main.handleLogin(loginTextField.getText(), passTextField.getText());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setMain(Main main) {
        this.main = main;
    }
}
