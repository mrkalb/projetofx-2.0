/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.AlertBox;
import DTO.Produto;
import DTO.ProdutoWrapper;
import java.io.IOException;
import java.net.URL;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * FXML Controller class
 *
 * @author igor
 */
public class MenuController implements Initializable {

    Main main;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    private void handleEstoque() throws IOException {
        main.mainView.setCenter(main.loadEstoque());
    }

    @FXML
    private void criaProduto() throws IOException {
        main.mainView.setCenter(main.loadProduto());
    }

    @FXML
    private void criaVenda() throws IOException {
        estoqueZerado();
        main.getCarrinhoData().clear();
        main.getTempProdutoVendaData().clear();
        main.mainView.setCenter(main.loadVenda());
    }

    @FXML
    private void about() throws IOException{
        main.mainView.setCenter(main.loadAbout());
    }
    
    @FXML
    public void apagaProduto() {
        main.getProdutoData().clear();
        main.getEstoqueData().clear();
    }

    @FXML
    public void apagaVenda() {
        for(Produto prd : main.getProdutoData()){
            prd.getEstoque().setQuantAtual(prd.getQuantidadeDo());
        }
        main.getVendaData().clear();
    }


    @FXML
    public void salvarProduto( ) throws JAXBException{
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            main.salvarProd(file);
        }
    }

    private void estoqueZerado(){
                ObservableList<Produto> temp = FXCollections.observableArrayList(); 
        for(int i = 0; i< main.getProdutoData().size();i++){
            if(main.getProdutoData().get(i).getEstoque().getQuantAtual() < 2){
                temp.addAll(main.getProdutoData().get(i));
            }
        }
        if(!temp.isEmpty()){
            for(int i =0;i<temp.size();i++){
                AlertBox.display("Estoque acabando","Os produtos abaixo tem estoque abaixo de 2\n" + temp.get(i).getDescricao());
            }
        }
    }
       
    
}
